<?php

namespace App\Utils;

class EncryptionUtil{

    public function __construct(){
        $this->rsa = new \phpseclib\Crypt\RSA();
        $this->createKeys();
    }

    private function createKeys(){
        $keys = $this->rsa->createKey(2048);
        if (!file_exists('key.pri')){
            file_put_contents('key.pri',$keys['privatekey']);
            file_put_contents('key.pub',$keys['publickey']);
        }
       
    }
    
    public function getServerPublicKey(){
        return file_get_contents('key.pub');
    }

    private function getServerPrivateKey(){
        return file_get_contents('key.pri');
    }

    function encrypt($plaintext,$asym_key){
        $this->rsa->loadKey($asym_key);         
	    //$rsa->setEncryptionMode(RSA::ENCRYPTION_OAEP);
        $ciphertext = $this->rsa->encrypt($plaintext);    
        return $ciphertext;
    }
    
    function decrypt($cyphertext, $asym_key){
        $this->rsa->loadKey($asym_key);         
	    $this->rsa->setEncryptionMode(\phpseclib\Crypt\RSA::ENCRYPTION_OAEP);
        $plaintext = $this->rsa->decrypt($cyphertext);    
        return $plaintext;
    }

   
}