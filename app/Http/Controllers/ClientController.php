<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ClientService;
use App\Utils\EncryptionUtil;

class ClientController extends Controller
{
    protected $clientService;
 
	public function __construct(ClientService $clientService)
	{
		$this->clientService = $clientService;
    }
    
    public function register(Request $request)
    {
        $userName = $request['userName'];
        $publicKey = $request['publicKey'];
        
        return $this->clientService->saveClient($userName, $publicKey);
    }
}
