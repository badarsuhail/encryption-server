<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ClientService;
use App\Services\ClientSecretsService;
use App\Utils\EncryptionUtil;

class SecretsController extends Controller
{
    protected $secretsService;
 
	public function __construct(ClientService $clientService, ClientSecretsService $secretsService)
	{
        $this->clientService = $clientService;
		$this->secretsService = $secretsService;
    }

    public function getServerKey()
    {
        return $this->secretsService->getServerPublicKey();
    }

    public function storeSecret(Request $request)
    {
        $userName = $request['userName'];
        $secretName = $request['secretName'];
        $encryptedSecret = $request['encryptedSecret'];
        $signature = $request['signature'];
        
        $client = $this->clientService->findByUserName($userName);
        $this->secretsService->storeSecret($client, $secretName, $encryptedSecret, $signature);
        
    }

    public function getSecret(Request $request)
    {
        $userName = $request['userName'];
        $secretName = $request['secretName'];
        $signature = $request['signature'];

        $client = $this->clientService->findByUserName($userName);

        return $this->secretsService->findBySecretName($client->id, $secretName);
    }
}
