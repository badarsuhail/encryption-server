<?php

namespace App\Repositories;
 
use App\Client;
 
class ClientRepository{
  
  protected $model;
 
  public function __construct(Client $model){
    $this->model = $model;
  }

  public function save($userName, $publicKey){
    return Client::updateOrCreate([
      'userName'   => $userName,
    ],[
      'userName'   => $userName,
      'publicKey'   => $publicKey
    ]);
  }
   
  public function findByUserName($userName){
    return Client::where('userName', $userName)->first();
  }
 
  public function update($id, array $attributes){
    return $this->model->find($id)->update($attributes);
  }
 
}