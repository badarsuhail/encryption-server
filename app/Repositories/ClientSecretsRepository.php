<?php

namespace App\Repositories;
 
use App\ClientSecrets;
 
class ClientSecretsRepository{
  
  protected $model;
 
  public function __construct(ClientSecrets $model){
    $this->model = $model;
  }

  public function save($clientId, $secretName, $secret){
    return ClientSecrets::updateOrCreate([
      'client_id' => $clientId,
      'secretName' => $secretName
    ],[
      'client_id' => $clientId,
      'secretName' => $secretName,
      'secret' => $secret
    ]);
  }
   
  public function findBySecretName($clientId, $secretName){
    return ClientSecrets::where('client_id', $clientId)
                ->where('secretName', $secretName)
                ->first();
  }
  
  public function update($id, array $attributes){
    return $this->model->find($id)->update($attributes);
  }
 
}