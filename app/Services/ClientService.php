<?php

namespace App\Services;
 
use App\Client;
use App\Repositories\ClientRepository;
use Illuminate\Http\Request;
use App\Utils\EncryptionUtil;
 
class ClientService{

    public function __construct(ClientRepository $clientRepository, EncryptionUtil $crypto){
        $this->clientRepository = $clientRepository ;
        $this->crypto = $crypto;
	}
 
    public function saveClient(String $userName, String $publicKey){
        return $this->clientRepository->save($userName, $publicKey);
	}
 
    public function findByUserName($userName){
        return $this->clientRepository->findByUserName($userName);
    }

	public function read($id){
        return $this->clientRepository->find($id);
	}
 
	public function update(Request $request, $id){
	    $attributes = $request->all();
        return $this->clientRepository->update($id, $attributes);
	}
 
	
}