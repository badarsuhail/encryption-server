<?php

namespace App\Services;
 
use App\UserSecrets;
use App\Repositories\ClientSecretsRepository;
use Illuminate\Http\Request;
use App\Utils\EncryptionUtil;
 
class ClientSecretsService{

    public function __construct(ClientSecretsRepository $repo, EncryptionUtil $crypto){
        $this->repo = $repo ;
        $this->crypto = $crypto;
	}
 
	public function getServerPublicKey(){
		return $this->crypto->getServerPublicKey();
	}
 
    public function save($clientId, $secretName, $secret){      
        return $this->repo->save($clientId, $secretName, $secret);
	}
 
	public function findBySecretName($clientId, $secretName){
        return $this->repo->findBySecretName($clientId, $secretName);
	}
 
	public function storeSecret($client, $secretName, $encryptedSecret, $signature){
        if ($this->crypto->decrypt($signature, $client->publicKey)){
			$this->save($client->id, $secretName, $encryptedSecret);
		}
	}

 
	
}