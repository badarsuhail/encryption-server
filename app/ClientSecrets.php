<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientSecrets extends Model
{
    protected $fillable = ['client_id', 'secretName', 'secret'];
}
