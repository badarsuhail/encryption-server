<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('client/register', 'ClientController@register');
Route::get('secret/getServerKey', 'SecretsController@getServerKey');
Route::post('secret/storeSecret', 'SecretsController@storeSecret');
Route::get('secret/getSecret/{userName}/{secretName}/{signature}', 'SecretsController@getSecret');
